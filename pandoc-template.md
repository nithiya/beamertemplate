---
title: There Is No Largest Prime Number
date: \today{}
---

# Some Examples

## Paragraphs

Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Ut purus elit, vestibulum ut, placerat
ac, adipiscing vitae, felis. Curabitur dictum gravida mauris. Nam arcu libero, nonummy eget,
consectetuer id, vulputate a, magna. Donec vehicula augue eu neque.
Nam dui ligula, fringilla a, euismod sodales, sollicitudin vel, wisi. Morbi auctor lorem non justo.

Nam lacus libero, pretium at, lobortis vitae, ultricies et, tellus. Donec aliquet, tortor sed
accumsan bibendum, erat ligula aliquet magna, vitae ornare odio metus a mi. Morbi ac orci et
nisl hendrerit mollis.

<!-- -------------------------------------------------------------------- -->
## Lists

- Nulla malesuada porttitor diam.

- Donec felis erat, congue non, volutpat at, tincidunt tristique, libero.

  - Phasellus adipiscing semper elit.
  - Maecenas lacinia.

1.  Vivamus viverra fermentum felis.

2.  Donec nonummy pellentesque ante.

    1.  Proin fermentum massa ac quam.
    2.  Sed diam turpis, molestie vitae, placerat a, molestie nec, leo.

<!-- -------------------------------------------------------------------- -->
## Text Blocks

#### Block

Quisque ullamcorper placerat ipsum. Cras nibh. Morbi vel justo vitae lacus tincidunt ultrices.

- Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

#### Example Block {.example}

In hac habitasse platea dictumst. Integer tempus convallis augue. Etiam facilisis.

- Nunc elementum fermentum wisi.

#### Alert Block {.alert}

Vivamus quis tortor vitae risus porta vehicula.

- Nunc vitae tortor. Proin tempus nibh sit amet nisl.

<!-- -------------------------------------------------------------------- -->
## Citations

These are some inline citations:

Article [@.sigfridsson]. Online [@.ctan]. Book [@.aristotle:anima]. Thesis [@.geer]. Report [@.padhye]. Proceedings [@.moraux].

<!-- -------------------------------------------------------------------- -->
# More Examples

## Figures

![Eurasian tree sparrow (*Passer montanus malaccensis*), adult male, in Kuala Lumpur, Malaysia. Taken on 31 January 2019, 15:20:47 by Peter P. Othagoer (Wikimedia Commons, CC BY 4.0).](images/Passer_montanus_malaccensis.jpg){ width=50% }

<!-- -------------------------------------------------------------------- -->
## Tables

Letter | Number | Symbol
--- | ---: | :---:
A / a | 1 | !
B / b | 2 | @
C / c | 3 | #
D / d | 4 | $
E / e | 5 | %

<!-- -------------------------------------------------------------------- -->
## Mathematics and Equations

Here's an example inline equation: $\textrm{e}^{i\pi} + 1 = 0$.

More equations:

$$\int x^n\,\mathrm{d}x = \frac{1}{n + 1}x^{n + 1}, \qquad n \neq -1$$

$$\sin \frac{\beta}{2} = \sqrt{\frac{1 - \cos \beta}{2}}$$

$$
\begin{bmatrix}
  n + 1 \\
  m + 1
\end{bmatrix}
= \sum_k
\begin{pmatrix}
  n \\
  k
\end{pmatrix}
\begin{bmatrix}
  k \\
  m
\end{bmatrix}
= \sum^n_{k = 0}
\begin{bmatrix}
  k \\
  m
\end{bmatrix}
(m + 1)^{n -k}
$$

<!-- -------------------------------------------------------------------- -->
## Code Blocks {frameoptions="allowframebreaks,fragile"}

#### Python {.example}

```py
r = requests.get(url)
if r.status_code == 200:
    try:
        z = ZipFile(BytesIO(r.content))
        z.extractall("download/path/")
        print("Data successfully downloaded!")
    # exception for bad zip file
    except BadZipFile:
        print("Data not extracted! Bad zip file.")
else:
    print("Error! Data not downloaded. Status code:", r.status_code)
```

#### Shell {.example}

```sh
#!/bin/sh

# change to home directory
cd $HOME

# delete all lines containing string in file
grep -v "string to delete" file.txt > tempfile.txt

# rename file
mv tempfile.txt file.txt
```

<!-- -------------------------------------------------------------------- -->
# References {.allowframebreaks}

```{=latex}
\setbeamerfont{bibliography}{size=\tiny}
\usebeamerfont{bibliography}
```
